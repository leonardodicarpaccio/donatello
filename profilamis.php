<?php
require_once('conn.php');

$Id=8;

$sql="   SELECT * 
        FROM User  
        WHERE  idUser='$Id'
        ";

        $r=mysqli_query($con,$sql); 
         $result['Apropos']['Fond']['nom']['photoProfil']['Id']=array();
        
        while($rw=mysqli_fetch_array($r)){            
            array_push($result,array(
			"Apropos"=>$rw['aPropos'],
			"nom"=>$rw['nom'],
			"photoProfil"=>$rw['photoProfil'],
            "Fond"=>$rw['photoDeFond'],
            "Id"=>$rw['idUser']
            ));
?>


<!DOCTYPE html>
<html>
    <header>
        <title>Donatello</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="bootstrap.min.css">
        <link rel="stylesheet" href="main.css">
    </header>
    <body class="general">

    <!-- Navbar -->
  <nav class="navbar navbar-default bresson none">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed white" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand home" href="profile.php"><i class="fa fa-home"></i> Donatello</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav white">
        <li><a href="profile.php"><i class="fa fa-user"></i><span class="sr-only">(current)</span></a></li>
        <li><a href="#"><i class="fa fa-envelope"></i></a></li>
        <li><a href="amis.php"><i class="fa fa-users"></i><span class="sr-only">(current)</span></a></li>
        <li><a href="#"><i class="fa fa-bell"></i></a></li>
      </ul>
      <form class="navbar-form navbar-left">
        <div class="form-group">
          <input type="text" class="form-control" placeholder="Recherche">
        </div>
        <button type="submit" class="btn btn-danger"><i class="fa fa-search"></i></button>
      </form>
      <ul class="nav navbar-nav navbar-right">
        <li>            
            <a href="deconnect.php" >
        <i class="fa fa-power-off" id="deco"></i></a>       
          </li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
        
        <div class="container-fluid padnull">
        <div class="container-fluid bg" style="background-image:url(<?php echo "".$rw['photoDeFond']."" ; ?>);">
            <br/><br/><br/>
        <div class="container">            
            <div class="row profile-hero">
                <div class="col-xs-6 col-md-4">
                    <img src="<?php echo "".$rw['photoProfil']."" ; ?>" alt="profile pic" style="width:100%">
                </div>
                <div class="col-xs-6 col-md-8" >
                    <h3><?php echo $rw['nom'];?></h3>
                    <hr>
                    <h4>A propos</h4>
                    <p>
                    <?php echo "".$rw['aPropos'].""; ?>
                    </p>
                    <hr>                   
                </div>
            </div>
                
        </div>
            <br/>
            <br/>
            <br/>
            </div>
            

            <div class="container">
            <div class="row">
                <div class="col-xs-2">
                    <div class="row">
                        <div class="row">
                            <h5><a href="amis.php">Amis</a></h5>
                        </div>
                        <!--Boucle d'affichage des amis (5 premiers par exemple)-->
                            <?php 
        
        $sql="   SELECT * 
        FROM User 
        INNER JOIN Amis 
        ON Amis.`#idUser1` = '$rw[idUser]' 
        WHERE  User.`idUser` = Amis.`#idUser2` AND Amis.`#idUser2` != '$rw[idUser]'
        ";
                    
        
        
                               
        $r=mysqli_query($con,$sql); 
         $result['Apropos']['nom']['photoProfil']=array();
        
        while($row=mysqli_fetch_array($r)){            
            array_push($result,array(
			"Apropos"=>$row['aPropos'],
			"nom"=>$row['nom'],
			"photoProfil"=>$row['photoProfil']
            ));
            ?>
                        <div class="row ami">
                            <div class="col-xs-4">
                                <img src="<?php echo $row['photoProfil']; ?>" alt="profile pic" style="width:100%">
                            </div>
                            <div class="col-xs-8 info">
                                <a href="profilamis.php"><b><?php echo($row['nom']);?></b></a>       <br>
                                <span><?php echo($row['aPropos']);?></span>
                            </div>
                        
                        </div>
                   
        <?php   
            
        }
        ?>
                         </div>
                            
                </div>
                <div class="col-xs-10 events">
                    <!--Evenements-->
                    <div class="row">
                        <h3 class="title">Actualités</h3>
                        <hr>
                    </div>
                    <!-- Type evenement-->
                    <?php 
        
        $sql="   SELECT * FROM Publication INNER JOIN Amis ON Publication.`#idUser` = Amis.`#idUser1` WHERE Amis.`#idUser2` = '$rw[idUser]'  ";                    
        $r=mysqli_query($con,$sql); 
        
        
        $result['Nom']['Photovideo']['Lieu']['Date']=array();
        
        while($row=mysqli_fetch_array($r)){
            
            
            
            array_push($result,array(
			
		
			"Photovideo"=>$row['Photovideo'],
			"Lieu"=>$row['Lieu'],
			"Date"=>$row['Date'],
            "Nom"=>$row['Description'],
            "Humeur"=>$row['Humeur']
			
			
		));
            ?>  

                    <div class="row event">
                        <div class="col-xs-2 info">
                            <span>A <?php echo($row['Lieu']);?></span><br>
                            <span>Le <?php echo($row['Date']);?></span><br>
                            <span>Humeur: <?php echo($row['Humeur']);?></span><br>
                        </div>
                        <div class="col-xs-10">
                            <div class="row">
                                <h4><?php echo($row['Description']);?></h4><hr>
                                <img src="<?php echo "".$row['Photovideo'].""; ?>" alt="profile pic" style="width:100%">
                            </div>
                        </div>
                        <hr>
                    </div>
                    <?php   
            
        }
        ?>
                </div>
            </div>

        </div>
        </div>     
        
        <?php
        }
    ?>
    </body>
</html>
