<?php
session_start();

require_once('conn.php');
session_destroy();


?>



<!DOCTYPE html>
<html>
    <header>
        <title>Donatello</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="bootstrap.min.css">
        <link rel="stylesheet" href="main.css">
        
        
    </header>
    <body class="general">
       
    <!-- Navbar -->
 <nav class="navbar navbar-default bresson">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed white" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand home" href="#"><i class="fa fa-home"></i> Donatello</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav white">
        <li><a href="#"><i class="fa fa-user"></i><span class="sr-only">(current)</span></a></li>
        <li><a href="#"><i class="fa fa-envelope"></i></a></li>
        <li><a href="#"><i class="fa fa-users"></i><span class="sr-only">(current)</span></a></li>
        <li><a href="#"><i class="fa fa-bell"></i></a></li>
      </ul>
      <form class="navbar-form navbar-left">
        <div class="form-group">
          <input type="text" class="form-control" placeholder="Recherche">
        </div>
        <button type="submit" class="btn btn-danger"><i class="fa fa-search"></i></button>
      </form>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="#"><i class="fa fa-power-off"></i></a></li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
     <!-- Container -->
        <div class="col-lg-4 col-md-4 col-sm-1 col-xs-1 "></div>
        <div class="col-lg-4 col-md-4 col-sm-10 col-xs-10 padnull">
        <div class='circle-multiline'>
            <div class="wrapper black">
                <form class="form-signin" action="profile.php" method="post">       
                  <h2 class="form-signin-heading">CONNEXION</h2>
                  <div class="pad"> Adresse email </div><input type="text" class="form-control " name="username" placeholder="Email Address" required="" autofocus="" />
                  <div class="pad"> Mot de passe </div><input type="password" class="form-control" name="password" placeholder="Password" required=""/>      
                  <label class="checkbox right">
                    <input type="checkbox" value="remember-me" id="rememberMe" name="rememberMe"> Remember me
                  </label>
                  <button class="btn btn-lg btn-warning btn-block home eight md" type="submit">Login</button> 
                
                </form>
            </div>
            </div>
            <br/>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-1 col-xs-1 "></div>
        <div class="col-lg-5 col-md-5 col-sm-1 col-xs-1 "></div>
        <div class="col-lg-2 col-md-2 col-sm-10 col-xs-10 ">
        <a href="inscription.php" class=""><button class="btn btn-lg btn-warning btn-block eight" type="submit">Inscription</button></a>
        </div>
        <div class="col-lg-5 col-md-5 col-sm-1 col-xs-1 "></div>
    </body>
</html>