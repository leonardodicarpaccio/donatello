<?php
session_start();

require_once('conn.php');
session_destroy();


?>



<!DOCTYPE html>
<html>
    <header>
        <title>Donatello</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="bootstrap.min.css">
        <link rel="stylesheet" href="main.css">
        
        
    </header>
    <body class="general">
       
    <!-- Navbar -->
 <nav class="navbar navbar-default bresson">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed white" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand home" href="#"><i class="fa fa-home"></i> Donatello</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav white">
        <li><a href="#"><i class="fa fa-user"></i><span class="sr-only">(current)</span></a></li>
        <li><a href="#"><i class="fa fa-envelope"></i></a></li>
        <li><a href="#"><i class="fa fa-users"></i><span class="sr-only">(current)</span></a></li>
        <li><a href="#"><i class="fa fa-bell"></i></a></li>
      </ul>
      <form class="navbar-form navbar-left">
        <div class="form-group">
          <input type="text" class="form-control" placeholder="Recherche">
        </div>
        <button type="submit" class="btn btn-danger"><i class="fa fa-search"></i></button>
      </form>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="#"><i class="fa fa-power-off"></i></a></li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
     <!-- Container -->
        <div class="col-lg-3 col-md-3 col-sm-1 col-xs-1"></div>
        <div class="col-lg-6 col-md-6 col-sm-10 col-xs-10">
            <div class="panel panel-warning ">
            <div class="panel-heading home center">Inscription</div>
                <div class=" panel-body wrapper">
                <form class="form-signin" method="post" action="newuser.php" enctype="multipart/form-data">       
                    <i class="fa fa-user iconform"></i><input type="text" class="form-control inscription" name="nom" placeholder="Nom" required="" autofocus="" /> <hr>
                  <i class="fa fa-envelope iconform"></i><input type="text" class="form-control inscription" name="mail" placeholder="Email Address" required="" autofocus="" /> <hr>
                  <i class="fa fa-key iconform"></i><input type="text" class="form-control inscription" name="pseudo" placeholder="Pseudo" required=""/>   <hr>
                    
                  <i class="fa fa-picture-o iconform"></i><input class="inscription" type="file" name="profilepic"/><hr>
                  <button class="btn btn-lg btn-wanted btn-block home" type="submit">Inscription</button>   
                </form>
            </div>
            </div>
        </div>
        
        
        <div class="col-lg-3 col-md-3col-sm-1 col-xs-1"></div> 
        
    </body>
</html>