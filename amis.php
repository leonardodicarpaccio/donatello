<?php
session_start();

require_once('conn.php');



  if(!isset($_SESSION['Id']) || empty($_SESSION['Id'])){

        

    session_destroy();

 header('Location: bs.php');
           exit();




}
?>


<!DOCTYPE html>
<html>
    <header>
        <title>Donatello</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="bootstrap.min.css">
        <link rel="stylesheet" href="main.css">
    </header>
    <body class="general">

  <nav class="navbar navbar-default bresson none">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed white" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand home" href="profile.php"><i class="fa fa-home"></i> Donatello</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav white">
        <li><a href="profile.php"><i class="fa fa-user"></i><span class="sr-only">(current)</span></a></li>
        <li><a href="#"><i class="fa fa-envelope"></i></a></li>
        <li><a href="amis.php"><i class="fa fa-users"></i><span class="sr-only">(current)</span></a></li>
        <li><a href="#"><i class="fa fa-bell"></i></a></li>
      </ul>
      <form class="navbar-form navbar-left">
        <div class="form-group">
          <input type="text" class="form-control" placeholder="Recherche">
        </div>
        <button type="submit" class="btn btn-danger"><i class="fa fa-search"></i></button>
      </form>
      <ul class="nav navbar-nav navbar-right">
        <li>            
            <a href="deconnect.php" >
        <i class="fa fa-power-off" id="deco"></i></a>       
          </li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
        
<div class="container">
    <div class="row">
        <h1>Amis</h1>
        <hr>
    </div>
    <div class="row">
        <!--Boucle d'affichage des amis-->
            <?php 
        
        $sql="   SELECT * 
        FROM User 
        INNER JOIN Amis 
        ON Amis.`#idUser1` = '$_SESSION[Id]' 
        WHERE  User.`idUser` = Amis.`#idUser2` AND Amis.`#idUser2` != '$_SESSION[Id]'
        ";
                    
        
        
                               
        $r=mysqli_query($con,$sql); 
         $result['Apropos']['nom']['photoProfil']=array();
        
        while($row=mysqli_fetch_array($r)){            
            array_push($result,array(
			"Apropos"=>$row['aPropos'],
			"nom"=>$row['nom'],
			"photoProfil"=>$row['photoProfil']
            ));
            ?>
            <div class="col-xs-3 ami">
            <div class="col-xs-4">
                <img src="<?php echo $row['photoProfil']; ?>" alt="profile pic" style="width:100%">
            </div>
            <div class="col-xs-8 info">
                <b><?php echo($row['nom']);?></b><hr>
                
                <span><?php echo($row['aPropos']);?></span>
            </div>
           
        </div>
        <?php   
            
        }
        ?>
         
    </div>
    
</div>
</body>
</html>
