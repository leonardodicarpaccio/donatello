<!DOCTYPE html>
<html>
    <header>
        <title>Donatello</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="bootstrap.min.css">
        <link rel="stylesheet" href="main.css">
        
    </header>
    <body class="general">
    <div class="panel panel-primary">
    <div class="panel-heading">
        <h3 class="panel-title">301 Moved Permanently</h3>
    </div>
    <div class="panel-body">The requested page has been permanently moved to a new location.</div>
</div>
    </body>
</html>