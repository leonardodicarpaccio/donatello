<?php

require_once('session.php');
require_once('conn.php');


?>


<!DOCTYPE html>
<html>
    <header>
        <title>Donatello</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="bootstrap.min.css">
        <link rel="stylesheet" href="main.css">
    </header>
    <body class="general">

    <!-- Navbar -->
 <nav class="navbar navbar-default bresson none">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed white" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand home" href="profile.php"><i class="fa fa-home"></i> Donatello</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav white">
        <li><a href="profile.php"><i class="fa fa-user"></i><span class="sr-only">(current)</span></a></li>
        <li><a href="#"><i class="fa fa-envelope"></i></a></li>
        <li><a href="amis.php"><i class="fa fa-users"></i><span class="sr-only">(current)</span></a></li>
        <li><a href="#"><i class="fa fa-bell"></i></a></li>
      </ul>
      <form class="navbar-form navbar-left">
        <div class="form-group">
          <input type="text" class="form-control" placeholder="Recherche">
        </div>
        <button type="submit" class="btn btn-danger"><i class="fa fa-search"></i></button>
      </form>
      <ul class="nav navbar-nav navbar-right">
        <li>            
            <a href="deconnect.php" >
        <i class="fa fa-power-off" id="deco"></i></a>       
          </li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>

        <div class="container-fluid padnull">
        <div class="container-fluid bg" style="background-image:url(<?php echo "".$_SESSION['Fond']."" ; ?>);">
            <form class="form-signin" method="post" action="changerfond.php" enctype="multipart/form-data">       
                <input class="inscription" type="file" name="cover"/>
                <button class="btn btn-wanted home" type="submit"><i class="fa fa-plus"></i></button>   
                </form>
        <div class="container">
            
            <div class="row">
                <h1 class="white">Mon Profil</h1>
                <hr>
            </div>
            
            <div class="row profile-hero">
                <div class="col-xs-6 col-md-4">
                    <img src="<?php echo "".$_SESSION['Profil']."" ; ?>" alt="profile pic" style="width:100%">
                    <form class="form-signin" method="post" action="changerpp.php" enctype="multipart/form-data">       
                <input type="file" name="pp"/>
                <button class="btn btn-wanted home right" type="submit"><i class="fa fa-plus"></i></button>   
                </form>
                </div>
                
                <div class="col-xs-6 col-md-8" >
                    <h3><?php echo $_SESSION['Nom'];?></h3>
                    <hr>
                    <h4>A propos</h4>
                    <p>
                    <?php echo "".$_SESSION['Propos'].""; ?>
                    </p>
                    <hr>
                            <form class="form" method="post" action="apropos.php">
                                <input type="text" class="form-control midleft" name="apropos" id="apropos">
                                <button type="submit" class="btn btn-warning right btn_summit"> Soumettre </button>
                            </form>
                    
                </div>
            </div>
                
        </div>

            <br/>
            <br/>
            </div>
            

            <div class="container">
            <div class="row">
                <div class="col-xs-2">
                    <div class="row">
                        <div class="row">
                            <h5><a href="amis.php">Amis</a></h5>
                        </div>
                        <!--Boucle d'affichage des amis (5 premiers par exemple)-->
                            <?php 
        
        $sql="   SELECT * 
        FROM User 
        INNER JOIN Amis 
        ON Amis.`#idUser1` = '$_SESSION[Id]' 
        WHERE  User.`idUser` = Amis.`#idUser2` AND Amis.`#idUser2` != '$_SESSION[Id]'
        ";
                    
        
        
                               
        $r=mysqli_query($con,$sql); 
         $result['Apropos']['nom']['photoProfil']=array();
        
        while($row=mysqli_fetch_array($r)){            
            array_push($result,array(
			"Apropos"=>$row['aPropos'],
			"nom"=>$row['nom'],
			"photoProfil"=>$row['photoProfil']
            ));
            ?>
                        <div class="row ami">
                            <div class="col-xs-4">
                                <img src="<?php echo $row['photoProfil']; ?>" alt="profile pic" style="width:100%">
                            </div>
                            <div class="col-xs-8 info">
                                <a href="profilamis.php"><b><?php echo($row['nom']);?></b></a>       <br>
                                <span><?php echo($row['aPropos']);?></span>
                            </div>
                        
                        </div>
                   
        <?php   
            
        }
        ?>
                         </div>
                            
                </div>
                <div class="col-xs-10 events">
                    <!--Evenements-->
                    <div class="row">
                        <h3 class="title">Actualités</h3>
                        <hr>
                    </div>
                    
                    <!--Formulaire d'ajout d'evenement-->
                    <div class="row">
                        <form method="post" action="ajoutelement.php" enctype="multipart/form-data">
                            <h5>Ajouter un évenement</h5>
                            <div class="form-group col-xs-8">
                                <label>Description</label>
                                <input type="text" name="description" class="form-control">
                            </div>
                            <div class="form-group col-xs-4">
                                <label>Humeur</label>
                                <input type="text" name="humeur" class="form-control">
                            </div>
                            <div class="form-group col-xs-4">
                                <label>Lieu</label>
                                <input type="text" name="lieu" class="form-control">
                            </div>
                            <div class="form-group col-xs-4">
                                <label>Date</label>
                                <input type="text" name="date" class="form-control">
                            </div>
                            <div class="form-group col-xs-4">
                                <label>Importer un fichier</label>
                                <input type="file" name="file" class="from-control">
                            </div>
                            <button class="btn btn-primary form-control" type="submit">Envoyer</button>
                        </form>
                        <hr>
                    </div>
                    <!-- Type evenement-->
                    <?php 
        
        $sql="   SELECT * FROM Publication INNER JOIN Amis ON Publication.`#idUser` = Amis.`#idUser1` WHERE Amis.`#idUser2` = '$_SESSION[Id]'  ";                    
        $r=mysqli_query($con,$sql); 
        
        
        $result['Nom']['Photovideo']['Lieu']['Date']=array();
        
        while($row=mysqli_fetch_array($r)){
            
            
            
            array_push($result,array(
			
		
			"Photovideo"=>$row['Photovideo'],
			"Lieu"=>$row['Lieu'],
			"Date"=>$row['Date'],
            "Nom"=>$row['Description'],
            "Humeur"=>$row['Humeur']
			
			
		));
            ?>  

                    <div class="row event">
                        <div class="col-xs-2 info">
                            <span>A <?php echo($row['Lieu']);?></span><br>
                            <span>Le <?php echo($row['Date']);?></span><br>
                            <span>Humeur: <?php echo($row['Humeur']);?></span><br>
                        </div>
                        <div class="col-xs-10">
                            <div class="row">
                                <h4><?php echo($row['Description']);?></h4><hr>
                                <img src="<?php echo "".$row['Photovideo'].""; ?>" alt="profile pic" style="width:100%">
                            </div><hr>
                            <div class="row">
                                <form method="post">
                                <button class="btn btn-default like"><i class="fa fa-thumbs-o-up"></i></button>
                                <button class="btn btn-default love"><i class="fa fa-heart"></i></button>
                                <button class="btn btn-default smile"><i class="fa fa-smile-o"></i></button>
                                <button class="btn btn-default dislike"><i class="fa fa-thumbs-o-down"></i></button>
                                </form>
                            </div>
                        </div>
                    </div>
                    <?php   
            
        }
        ?>
                </div>
            </div>

        </div>
        </div>     
        
        
    </body>
</html>
