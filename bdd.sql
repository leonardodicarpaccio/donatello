-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Apr 20, 2017 at 02:23 AM
-- Server version: 5.6.35
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `donatello`
--

-- --------------------------------------------------------

--
-- Table structure for table `Admin`
--

CREATE TABLE `Admin` (
  `idAdmin` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `Amis`
--

CREATE TABLE `Amis` (
  `id` int(11) NOT NULL,
  `#idUser1` int(11) NOT NULL,
  `#idUser2` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Amis`
--

INSERT INTO `Amis` (`id`, `#idUser1`, `#idUser2`) VALUES
(1, 1, 2),
(2, 2, 1),
(3, 1, 1),
(4, 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `Commentaire`
--

CREATE TABLE `Commentaire` (
  `idCommentaire` int(11) NOT NULL,
  `Message` varchar(50) NOT NULL,
  `#idPublication` int(11) NOT NULL,
  `#idUser` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `Publication`
--

CREATE TABLE `Publication` (
  `idPublication` int(11) NOT NULL,
  `Photo/vidéo` varchar(100) NOT NULL,
  `Lieu` varchar(20) NOT NULL,
  `Date` datetime NOT NULL,
  `Autorisation` varchar(20) NOT NULL,
  `Type` varchar(20) NOT NULL,
  `#idUser` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Publication`
--

INSERT INTO `Publication` (`idPublication`, `Photo/vidéo`, `Lieu`, `Date`, `Autorisation`, `Type`, `#idUser`) VALUES
(1, 'bde', 'ece', '2017-04-11 00:00:00', '1', 'pic', 2),
(2, 'bds', 'valtho', '2017-04-19 00:00:00', '1', 'vid', 1);

-- --------------------------------------------------------

--
-- Table structure for table `Reaction`
--

CREATE TABLE `Reaction` (
  `idReaction` int(11) NOT NULL,
  `Reaction` tinyint(5) NOT NULL,
  `#idPublication` int(11) NOT NULL,
  `#idUser` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `User`
--

CREATE TABLE `User` (
  `idUser` int(11) NOT NULL,
  `pseudo` varchar(20) NOT NULL,
  `email` varchar(40) NOT NULL,
  `nom` varchar(20) NOT NULL,
  `photoDeFond` varchar(20) NOT NULL,
  `#idAdmin` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `User`
--

INSERT INTO `User` (`idUser`, `pseudo`, `email`, `nom`, `photoDeFond`, `#idAdmin`) VALUES
(1, 'monomakhoff', 'victor.monomakhoff@edu.ece.fr', 'Victor Monomakhoff', 'photos/bacterie.jpg', 1),
(2, 'copp', 'coppart@edu.ece.fr', 'Thibaut Coppart', 'photos/bacterie.jpg', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Admin`
--
ALTER TABLE `Admin`
  ADD PRIMARY KEY (`idAdmin`);

--
-- Indexes for table `Amis`
--
ALTER TABLE `Amis`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `Commentaire`
--
ALTER TABLE `Commentaire`
  ADD PRIMARY KEY (`idCommentaire`);

--
-- Indexes for table `Publication`
--
ALTER TABLE `Publication`
  ADD PRIMARY KEY (`idPublication`);

--
-- Indexes for table `Reaction`
--
ALTER TABLE `Reaction`
  ADD PRIMARY KEY (`idReaction`);

--
-- Indexes for table `User`
--
ALTER TABLE `User`
  ADD PRIMARY KEY (`idUser`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Admin`
--
ALTER TABLE `Admin`
  MODIFY `idAdmin` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `Amis`
--
ALTER TABLE `Amis`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `Commentaire`
--
ALTER TABLE `Commentaire`
  MODIFY `idCommentaire` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `Publication`
--
ALTER TABLE `Publication`
  MODIFY `idPublication` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `Reaction`
--
ALTER TABLE `Reaction`
  MODIFY `idReaction` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `User`
--
ALTER TABLE `User`
  MODIFY `idUser` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;